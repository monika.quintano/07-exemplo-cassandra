package br.com.itau.pinterest.models;

public class FotoServiceResposta {
	private Foto foto;
	private FotoUsuario fotoUsuario;
	
	public FotoServiceResposta(Foto foto, FotoUsuario fotoUsuario) {
		this.foto = foto;
		this.fotoUsuario = fotoUsuario;
	}
	
	public Foto getFoto() {
		return foto;
	}
	public void setFoto(Foto foto) {
		this.foto = foto;
	}
	public FotoUsuario getFotoUsuario() {
		return fotoUsuario;
	}
	public void setFotoUsuario(FotoUsuario fotoUsuario) {
		this.fotoUsuario = fotoUsuario;
	}
}
